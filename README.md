# The Brahms OAI-PMH Harvester

This repository contains the code for the Brahms OAI-PMH harvester. It harvests the
Naturalis [media library](https://medialib.naturalis.nl/) via the
[OAI-PMH server](https://oaipmh.naturalis.nl/medialib/).

The Brahms OAI-PMH Harvester is a stand-alone Java application that will bring up an
internal scheduler that will run the harvest every so many hours (configurable). In other
words, the Brahms OAI-PMH Harvester does not itself need to be scheduled via cron or so.
The application runs "forever".

## Harvest Interval And Look-Back Period

Apart from the harvest interval (**in hours**), you can also configure the number of
**days** the harvester will look back for new image arrivals in the
[media library](https://medialib.naturalis.nl/). By setting the look-back period two to
four times larger than the harvest interval, you ensure that Brahms stays up-to-date even
in case of incidental server outages. This will cause images to be harvested multiple
times (at most `look_back_period / harvest_interval` times), but the harvester will be
make sure that images are inserted only once in the Brahms database.

## Full Harvests

You can also leave look-back period empty. This will result in a full harvest. In this
case you _should_ stop the Brahms OAI-PMH Harvester once the harvest completes, otherwise
it keeps scheduling full harvests. Set the `RUN_ONCE` environment variable to `true` to
make the Brahms OAI-PMH Harvester exit automatically after a single harvest.

## Repair Harvests

You can also execute "repair harvests". To do so, set the `REPAIR` environment variable to
`true`. Repair harvests are harvests that are executed on a Brahms database from which all
medialib images have first been removed. You would most likely want to combine this with a
_full harvest_, although the program does not enforce this. This is essentially a
performance optimization setting. By setting `REPAIR` to `true`, you assure the harvester
there are no medialib images yet in the Brahms database, and therefore it knows it can
skip the duplicates check mentioned [above](#harvest-interval-and-look-back-period). This
dramatically increases performance, especially for full (or large) harvests.

When executing a repair harvest, the `RUN_ONCE` variable is ignored. The harvest will 
always run just once.

## Harvesting Multiple Datasets

For day-to-day, scheduled harvests, only the "Herbarium-Brahms-Algemeen" dataset needs to
be harvested. For a full harvest, the following datasets need to be harvested:

1. Herbarium-Brahms-Algemeen
2. Glasstraat-Brahms-Algemeen
3. HERBARIUMLEIDEN
4. HerbariumstraatLeiden-Brahms-Algemeen
5. HerbariumstraatWageningen-Brahms-Algemeen
6. HERBARIUMWAG
7. MELLON
8. Mellon-Brahms-Algemeen

## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks).
The pipeline is configured to scan on leaked secrets.

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

 * `pre-commit autoupdate`
 * `pre-commit install`

## Configuration

Input for the application is passed in via environment variables. See
[brahms-oaipmh.env](brahms-oaipmh.env) for which environment variables need to be set. The
HARVEST_INTERVAL_HOURS variable determines how often to harvest, while the LOOK_BACK_DAYS
determines how far to look back for new image arrivals. More precisely, it determines
the value of the OAI-PMH "from" parameter. It is calculated as
`now() minus LOOK_BACK_DAYS`.

## Java-only Build & Run

This paragraph describes how to build and run the application using just a JVM (without
Docker and/or Docker Compose).

Go to the `Build` directory inside this repository and run
[mvn-install.sh](docker/mvn-install.sh):

```shell
cd Build
mvn-install.sh
```

This will build the Maven artifact and copy it and all its dependencies to
docker/dependencies. Next, make sure the environment is set correctly. See
[ansible/roles/deploy/templates/env.j2](ansible/roles/deploy/templates/env.j2) 
for which environment variables need to be set. Then, while in the `docker` 
directory, run:

```shell
java -cp 'dependencies/*' -Xmn1024G -Dfile.encoding=UTF-8 nl.naturalis.brahms.oaipmh.Main
```

## Docker-only Build & Run

This paragraph describes how to build and run the application using only Docker (without
Docker Compose).

Go to the `docker` directory inside this repository and run
[mvn-install.sh](docker/mvn-install.sh):

```shell
cd docker
mvn-install.sh
```

This will build the Maven artifact and copy it and all its dependencies to
docker/dependencies.

Make a copy of brahms-oaipmh.env and call it .env. Set the environment values within .env
as appropriate. While in the `Build` directory, create a Docker image named
"brahms-oaipmh", a container named "brahms-oaipmh", and start the container:

```shell
# Delete original container & image if necessary:
docker container rm brahms-oaipmh
docker image rm brahms-oaipmh

docker image build --tag brahms-oaipmh .
docker container create --name brahms-oaipmh --env-file ../.env brahms-oaipmh
docker container start brahms-oaipmh
```

Verify the harvester is running:

```shell
docker container logs -f brahms-oaipmh
```

Harvests will also be recorded (and should be visible) in the
[oaipmh-monitor](https://chat.naturalis.io/ict-aob/channels/oaipmh-monitor)
channel of mattermost.

## Run with Docker Compose

This paragraph assumes that a docker image has been created in the
[Gitlab container registry](https://gitlab.com/naturalis/bii/brahms/brahms-oaipmh/container_registry)
(most likely via a gitlab CI pipeline, so also see [.gitlab-ci.yml](.gitlab-ci.yml)).

Make a copy of brahms-oaipmh.env and call it .env. Set the environment values within .env
as appropriate. Notably, set the DOCKER_IMAGE_TAG correctly.

While in the _root_ of the repository (containing the .env file
and [docker-compose.yml](docker-compose.yml)), run:

```shell
docker compose up -d
```

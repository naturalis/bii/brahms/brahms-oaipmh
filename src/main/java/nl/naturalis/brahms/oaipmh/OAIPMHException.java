package nl.naturalis.brahms.oaipmh;

final class OAIPMHException extends RuntimeException {
  OAIPMHException(String message) { super(message); }
}

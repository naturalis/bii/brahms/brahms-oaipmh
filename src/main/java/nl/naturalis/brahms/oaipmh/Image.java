package nl.naturalis.brahms.oaipmh;

import java.util.UUID;

public record Image(UUID imageid,
      String imagefile,
      String imageurl,
      String addedby,
      String addedon,
      UUID databaseprojectid,
      String comments) {

  private static final String COMMENTS = "Created by the Brahms OAIPMH harvester";

  Image(String imagefile, String addedby, String addedon, UUID databaseprojectid) {
    this(UUID.randomUUID(),
          imagefile,
          imagefile,
          addedby,
          addedon,
          databaseprojectid,
          COMMENTS);
  }


}

package nl.naturalis.brahms.oaipmh;

import org.klojang.util.MathMethods;
import org.klojang.util.MutableInt;
import org.klojang.util.StringMethods;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static nl.naturalis.brahms.oaipmh.DbUtil.findDuplicates;
import static nl.naturalis.brahms.oaipmh.DbUtil.loadSpecimens;
import static org.klojang.util.CollectionMethods.implode;
import static org.klojang.util.ObjectMethods.isEmpty;


final class Harvester {

  private static final Logger LOG = LoggerFactory.getLogger(Harvester.class);

  private static final DateTimeFormatter dtf
        = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

  static void harvestAndHandleExceptions() {
    long start = Instant.now().getEpochSecond();
    LOG.info("Harvest started at {}", dtf.format(LocalDateTime.now()));
    try {
      harvest();
      LOG.info("Harvest successful");
    } catch (Throwable t) {
      LOG.error(t.toString(), t);
      LOG.info("Harvest did not complete successfully");
    }
    long end = Instant.now().getEpochSecond();
    Duration duration = Duration.ofSeconds(end - start);
    String hours = StringMethods.lpad(duration.toHours(), 2, '0');
    String minutes = StringMethods.lpad(duration.toMinutesPart(), 2, '0');
    String seconds = StringMethods.lpad(duration.toSecondsPart(), 2, '0');
    LOG.info("Harvest finished at {} (took {}:{}:{})",
          dtf.format(LocalDateTime.now()),
          hours,
          minutes,
          seconds);
  }

  private static void harvest() throws
        URISyntaxException,
        IOException,
        InterruptedException,
        SQLException,
        SAXException {
    String url = OaiUtil.getInitialURL();
    LOG.info("******* Calling OAI-PMH server: {}", url);
    Document doc = OaiUtil.callOaipmhServer(url);
    Element root = doc.getDocumentElement();
    Element error = XMLUtil.getChild(root, "error");
    if (error != null) {
      handleError(error);
      return;
    }
    int processed = 0;
    int inserted = 0;
    MutableInt orphans = new MutableInt();
    MutableInt duplicates = new MutableInt();
    HarvestConfig config = HarvestConfig.instance();
    List<Image> images = new ArrayList<>(config.batchSize());
    List<ImageLink> links = new ArrayList<>(config.batchSize());
    List<String> barcodesOK = (config.repair() || config.lookBackSeconds()
          .isEmpty()) ? null : new ArrayList<>(100);
    LOG.trace("Connecting to BRAHMS database");
    try (Connection con = DbUtil.connect()) {
      LOG.trace("Retrieving database product id");
      UUID dbProductId = DbUtil.getDatabaseProjectId(con);
      for (int batch = 1; ; ++batch) {
        Element listRecordsElem = XMLUtil.getChild(root, "ListRecords");
        List<Element> recordElems = XMLUtil.getChildren(listRecordsElem, "record");
        LOG.trace("Received {} records from OAI-PMH server", recordElems.size());
        processed += recordElems.size();
        Element resTokenElem = XMLUtil.getChild(listRecordsElem, "resumptionToken");
        images.clear();
        links.clear();
        createBeans(con,
              recordElems,
              dbProductId,
              images,
              links,
              barcodesOK,
              orphans,
              duplicates);
        DbUtil.insertImages(con, images);
        DbUtil.insertImageLinks(con, links);
        inserted += images.size();
        logBatchStats(batch, resTokenElem, processed, inserted, orphans, duplicates);
        if (resTokenElem == null) {
          LOG.info("Ready");
          break;
        }
        url = OaiUtil.getResumptionURL(resTokenElem.getTextContent());
        LOG.info("Calling OAI-PMH server: {}", url);
        doc = OaiUtil.callOaipmhServer(url);
        root = doc.getDocumentElement();
        error = XMLUtil.getChild(root, "error");
        if (error != null) {
          handleError(error);
          break;
        }
      }
      if (!isEmpty(barcodesOK)) {
        LOG.info("The following images were added to Brahms: {}", implode(barcodesOK));
      }
    }
  }

  private static void createBeans(Connection con,
        List<Element> recordElems,
        UUID dbProductId,
        List<Image> images,
        List<ImageLink> links,
        List<String> barcodesOK,
        MutableInt orphanCounter,
        MutableInt duplicateCounter) {
    // we _do_ expect duplicates because the harvest interval is short relative to
    // the look-back interval
    Set<String> duplicates = findDuplicates(con, recordElems);
    List<String> duplicateBarcodes = new ArrayList<>(duplicates.size());
    Map<String, List<Specimen>> specimensByBarcode = loadSpecimens(con, recordElems);
    List<String> orphans = new ArrayList<>();
    for (Element recordElem : recordElems) {
      String barcode = XMLUtil.getDescendantValue(recordElem, "dc:description");
      String url = XMLUtil.getDescendantValue(recordElem, "dc:identifier");
      if (duplicates.contains(url)) {
        duplicateBarcodes.add(barcode);
        duplicateCounter.ppi();
        continue;
      }
      String specimenId = StringMethods.substringBefore(barcode, "_", 1);
      List<Specimen> specimens = specimensByBarcode.get(specimenId);
      if (specimens == null) {
        orphanCounter.ppi();
        orphans.add(specimenId);
        continue;
      }
      String producer = XMLUtil.getDescendantValue(recordElem, "dc:creator");
      String publishDate = XMLUtil.getDescendantValue(recordElem, "datestamp");
      Image image = new Image(url, producer, publishDate, dbProductId);
      images.add(image);
      specimens.stream()
            .map(sp -> new ImageLink(sp, image, barcode, dbProductId))
            .forEach(links::add);
      if (barcodesOK != null) {
        barcodesOK.add(barcode);
      }
    }
    if (!duplicateBarcodes.isEmpty()) {
      LOG.info("Ignored because image already in Brahms: {}", implode(duplicateBarcodes));
    }
    if (!orphans.isEmpty()) {
      LOG.warn("Ignored because barcode not in Brahms: {}", implode(orphans));
    }
  }

  private static void logBatchStats(int batchNo,
        Element resTokenElem,
        int processed,
        int inserted,
        MutableInt orphans,
        MutableInt duplicates) {
    int total = resTokenElem == null ? processed : Integer.parseInt(resTokenElem.getAttribute(
          "completeListSize"));
    int batches = MathMethods.divUp(total, HarvestConfig.instance().batchSize());
    int todo = batches - batchNo;
    LOG.info(
          "Batch {} done. {} more to go. Total number of records: {}. Processed: {}. Inserted: {}. Orphans: {}. Duplicates: {}.",
          batchNo,
          todo,
          total,
          processed,
          inserted,
          orphans,
          duplicates);
  }

  private static void handleError(Element error) {
    if (error.getAttribute("code").equals("noRecordsMatch")) {
      LOG.info("Nothing to harvest");
    } else {
      throw new OAIPMHException(error.getTextContent());
    }
  }


}

package nl.naturalis.brahms.oaipmh;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Entrypoint for the Brahms OAIPMH harvester. Sets up de scheduler for the harvest.
 */
public class Main {

  private static final Logger LOG = LoggerFactory.getLogger(Main.class);

  private static final HarvestConfig config = HarvestConfig.instance();

  public static void main(String[] args) {
    String version = config.imageVersion();
    LOG.info("Running version \"{}\" of the Brahms OAIPMH harvester", version);
    if (config.runOnce() || config.repair()) {
      LOG.info("Running in run-once mode");
      Harvester.harvestAndHandleExceptions();
    } else {
      LOG.info("Running in scheduled mode");
      try {
        startScheduler();
      } catch (Exception e) {
        LOG.error(e.toString(), e);
      }
    }
  }

  private static void startScheduler() throws SchedulerException {
    JobDetail job = JobBuilder.newJob(HarvestJob.class)
          .withIdentity("brahms-oaipmh-harvester", "brahms")
          .build();
    int interval = config.harvestIntervalSeconds();
    SimpleScheduleBuilder schedule = SimpleScheduleBuilder.simpleSchedule()
          .withIntervalInSeconds(interval)
          .repeatForever();
    Trigger trigger = TriggerBuilder.newTrigger()
          .withIdentity("brahms-oaipmh-trigger", "brahms")
          .startNow()
          .withSchedule(schedule)
          .build();
    Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
    scheduler.scheduleJob(job, trigger);
    scheduler.start();
  }

}

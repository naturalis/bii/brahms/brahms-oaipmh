package nl.naturalis.brahms.oaipmh;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public final class HarvestJob implements Job {
  @Override
  public void execute(JobExecutionContext ctx) throws JobExecutionException {
    Harvester.harvestAndHandleExceptions();
  }
}

package nl.naturalis.brahms.oaipmh;

import org.klojang.check.Check;
import org.klojang.convert.Bool;
import org.klojang.convert.NumberMethods;

import java.util.*;

import static java.lang.System.getenv;
import static java.util.stream.Collectors.toList;
import static org.klojang.check.CommonChecks.empty;
import static org.klojang.util.StringMethods.isBlank;

record HarvestConfig(String imageVersion,
      String oaipmhServerBaseUrl,
      List<String> datasets,
      int harvestIntervalSeconds,
      OptionalInt lookBackSeconds,
      String dbHost,
      int dbPort,
      String dbUser,
      String dbPass,
      String dbName,
      String logLevel,
      boolean runOnce,
      boolean repair,
      int batchSize) {

  private static final HarvestConfig INSTANCE = create();

  public static HarvestConfig instance() { return INSTANCE; }

  private static HarvestConfig create() {
    String imageVersion = getenv("IMAGE_VERSION");
    String oaipmhServerBaseUrl = getenv("OAIPMH_SERVER_BASE_URL");
    List<String> datasets = getDatasets();
    int harvestIntervalSeconds = getHarvestIntervalSeconds();
    OptionalInt lookBackSeconds = getLookBackSeconds();
    String dbHost = getenv("DB_HOST");
    int dbPort = getDbPort();
    String dbUser = getenv("DB_USER");
    String dbPass = getenv("DB_PASS");
    String dbName = getenv("DB_NAME");
    String logLevel = getenv("LOG_LEVEL");
    boolean runOnce = Bool.from(getenv("RUN_ONCE"));
    boolean repair = Bool.from(getenv("REPAIR"));
    int batchSize = NumberMethods.parseInt(getenv("BATCH_SIZE"));
    return new HarvestConfig(imageVersion,
          oaipmhServerBaseUrl,
          datasets,
          harvestIntervalSeconds,
          lookBackSeconds,
          dbHost,
          dbPort,
          dbUser,
          dbPass,
          dbName,
          logLevel,
          runOnce,
          repair,
          batchSize);
  }

  private static List<String> getDatasets() {
    String s = getenv("OAI_SET");
    if (isBlank(s)) {
      return Collections.emptyList();
    }
    return Arrays.stream(s.split(",")).map(String::strip).collect(toList());
  }

  private static int getHarvestIntervalSeconds() {
    String s = getenv("HARVEST_INTERVAL_HOURS");
    Check.that(s, "HARVEST_INTERVAL_HOURS").isNot(empty());
    double d = NumberMethods.parseDouble(s.strip());
    return (int) (d * 3600);
  }

  private static OptionalInt getLookBackSeconds() {
    String s = getenv("LOOK_BACK_DAYS");
    if (isBlank(s)) {
      return OptionalInt.empty();
    }
    double days = NumberMethods.parseDouble(s.strip());
    int seconds = (int) (days * 60 * 60 * 24);
    return OptionalInt.of(seconds);
  }

  private static int getDbPort() {
    String s = getenv("DB_PORT");
    if (isBlank(s)) {
      return 5432;
    }
    return NumberMethods.parseShort(s.strip());
  }


}

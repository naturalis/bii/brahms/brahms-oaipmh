package nl.naturalis.brahms.oaipmh;

import org.klojang.util.ExceptionMethods;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

final class XMLUtil {

  static Document read(InputStream xml) throws IOException, SAXException {
    return read(xml, false, false);
  }

  static List<Element> getChildren(Element parent) {
    NodeList nl = parent.getChildNodes();
    if (nl.getLength() == 0) return null;
    List<Element> elements = new ArrayList<>(nl.getLength());
    for (int i = 0; i < nl.getLength(); ++i) {
      if (nl.item(i) instanceof Element e) {
        elements.add(e);
      }
    }
    return elements;
  }

  /**
   * Returns all child elements of the specified element which have the specified tag
   * name. This method returns {@code null} if the specified element has no children of
   * type {@code Element}.
   */
  static List<Element> getChildren(Element parent, String tagName) {
    NodeList nl = parent.getChildNodes();
    if (nl.getLength() == 0) return null;
    List<Element> elements = new ArrayList<>(nl.getLength());
    for (int i = 0; i < nl.getLength(); ++i) {
      if (nl.item(i) instanceof Element e) {
        if (e.getTagName().equals(tagName)) {
          elements.add(e);
        }
      }
    }
    return elements;
  }

  /**
   * Returns the first child element of the specified element which has the specified tag
   * name.
   */
  static Element getChild(Element parent, String tagName) {
    for (Element e : getChildren(parent)) {
      if (e.getTagName().equals(tagName)) {
        return e;
      }
    }
    return null;
  }

  /**
   * Returns the first element descending from {@code ancestor} that has the specified
   * {@code tagName}.
   */
  static Element getDescendant(Element ancestor, String tagName) {
    NodeList nl = ancestor.getElementsByTagName(tagName);
    if (nl.getLength() == 0) {
      return null;
    }
    return (Element) nl.item(0);
  }


  static String getDescendantValue(Element ancestor, String tagName) {
    Element e = getDescendant(ancestor, tagName);
    return e == null ? null : e.getTextContent();
  }

  private static Document read(InputStream xml,
        boolean namespaceAware,
        boolean validating) throws IOException, SAXException {
    return docBuilder(namespaceAware, validating).parse(xml);
  }


  private static DocumentBuilder docBuilder(boolean namespaceAware, boolean validating) {
    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    dbf.setNamespaceAware(namespaceAware);
    dbf.setValidating(validating);
    try {
      return dbf.newDocumentBuilder();
    } catch (ParserConfigurationException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }


}

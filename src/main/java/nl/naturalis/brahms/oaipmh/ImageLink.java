package nl.naturalis.brahms.oaipmh;

import java.util.UUID;

public record ImageLink(UUID imagelinkid,
      UUID imagedatalink, // the specimen id (pfff ...)
      UUID imageid,
      UUID databaseprojectid,
      int moduleid,
      String modulename,
      String addedby,
      String addedon,
      String comments) {

  private static final String COMMENTS = "Created by the Brahms OAIPMH harvester";

  ImageLink(Specimen specimen, Image image, String barcode, UUID databaseprojectid) {
    this(UUID.randomUUID(),
          specimen.specimenid(),
          image.imageid(),
          databaseprojectid,
          60,
          "Specimens",
          image.addedby(),
          image.addedon(),
          COMMENTS);
  }
}

package nl.naturalis.brahms.oaipmh;

import org.apache.http.client.utils.URIBuilder;
import org.klojang.check.Check;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static java.net.http.HttpClient.newHttpClient;
import static java.net.http.HttpResponse.BodyHandler;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.klojang.check.CommonChecks.empty;
import static org.klojang.check.CommonExceptions.illegalState;
import static org.klojang.util.CollectionMethods.implode;

/**
 * Methods related to calling the OAIPMH server and processing OAIPMH XML.
 */
final class OaiUtil {

  private static final Logger LOG = LoggerFactory.getLogger(OaiUtil.class);

  private static final DateTimeFormatter dtf =
        DateTimeFormatter.ofPattern("uuuu-MM-dd'T'HH:mm:ss'Z'");

  /**
   * Returns the URL for the first call to the OAIPMH server
   */
  static String getInitialURL() {
    HarvestConfig config = HarvestConfig.instance();
    StringBuilder sb = new StringBuilder(200);
    sb.append(config.oaipmhServerBaseUrl()).append("/?verb=ListRecords")
          .append("&metadataPrefix=oai_dc")
          .append("&__pageSize=")
          .append(config.batchSize());
    if (config.lookBackSeconds().isEmpty()) {
      // Then we're doing a full harvest and we will likely have to
      // process a very large result set. To suppress using persistent
      // result sets, just specify a very large number for LOOK_BACK_DAYS
      // (e.g. 100,000 days, which will take you back to the fifties)
      sb.append("&__stayAlive=true");
    }
    if (config.repair()) {
      sb.append("&__ignoreDeleted=true");
    }
    List<String> sets = config.datasets();
    Check.that(sets).isNot(empty(), illegalState("OAI_SET variable not set"));
    String setParams = implode(sets, x -> "&set=" + x, "");
    sb.append(setParams);
    LOG.info("Will harvest {}", implode(sets));
    if (config.lookBackSeconds().isPresent()) {
      String fromParam = getFromDateURLEncoded();
      sb.append("&from=").append(fromParam);
      LOG.info("Will harvest from {}", getFromDate());
    } else {
      LOG.warn("LOOK_BACK_DAYS variable not set. Will do full harvest!");
    }
    LOG.info("Will harvest in batches of {} records", config.batchSize());
    return sb.toString();
  }

  static String getResumptionURL(String resumptionToken) {
    HarvestConfig config = HarvestConfig.instance();
    StringBuilder sb = new StringBuilder(200);
    sb.append(config.oaipmhServerBaseUrl()).append("/?verb=ListRecords");
    if (config.lookBackSeconds().isEmpty()) {
      sb.append("&__stayAlive=true");
    }
    if (config.repair()) {
      sb.append("&__ignoreDeleted=true");
    }
    sb.append("&resumptionToken=")
          .append(resumptionToken);
    return sb.toString();
  }

  static Document callOaipmhServer(String url)
        throws URISyntaxException, IOException, InterruptedException, SAXException {
    HttpRequest request = HttpRequest.newBuilder(new URI(url)).build();
    BodyHandler<String> bodyHandler = HttpResponse.BodyHandlers.ofString();
    String xml = null;
    try (HttpClient httpClient = newHttpClient()) {
      HttpResponse<String> response = httpClient.send(request, bodyHandler);
      xml = response.body();
      InputStream is = new ByteArrayInputStream(xml.getBytes(UTF_8));
      return XMLUtil.read(is);
    } catch (SAXParseException e) {
      LOG.error("Error parsing XML: " + e.getMessage());
      LOG.error("######################## BEGIN XML ########################");
      LOG.error("\n" + xml);
      LOG.error("######################### END XML #########################");
      throw e;
    }
  }

  private static String getFromDateURLEncoded() {
    HarvestConfig config = HarvestConfig.instance();
    int seconds = config.lookBackSeconds().getAsInt();
    LocalDateTime from = LocalDateTime.now().minusSeconds(seconds);
    // URL-encode
    String fromStr = new URIBuilder()
          .addParameter("x", dtf.format(from))
          .toString()
          .substring(3);
    return fromStr;
  }

  private static String getFromDate() {
    HarvestConfig config = HarvestConfig.instance();
    int seconds = config.lookBackSeconds().getAsInt();
    LocalDateTime from = LocalDateTime.now().minusSeconds(seconds);
    return dtf.format(from);
  }

}

package nl.naturalis.brahms.oaipmh;

import org.klojang.check.Check;
import org.klojang.jdbc.*;
import org.klojang.util.StringMethods;
import org.w3c.dom.Element;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;

import static java.util.stream.Collectors.toList;
import static org.klojang.check.CommonChecks.eq;
import static org.klojang.check.CommonExceptions.illegalState;
import static org.klojang.check.CommonProperties.size;
import static org.klojang.templates.NameMapper.AS_IS;
import static org.klojang.util.StringMethods.append;

/*
 * Mappings OAIPMH    => specimens table
 * -----------------------------------------------------------------------------------
 * o  dc:description  => specimenbarcode       the image regno, like WAG.1173707
 *
 *
 * Mappings OAIPMH    => images table
 * -----------------------------------------------------------------------------------
 * o  dc:identifier   => imagefile             the medialib URL
 * o  dc:creator      => addedby               the image producer, like Herbarium-Brahms-Algemeen
 * o  datestamp       => addedon
 *
 *
 * The "databaseprojectid" column (a UUID), present in images and imagelinks is the same
 * for every record in every table within one Brahms database instance. We  look it up
 * once, just before the harvest starts and use it for all records we insert ourselves.
 */
final class DbUtil {

  static final SessionConfig SESSION_CONFIG = SessionConfig.getDefaultConfig()
        .withColumnToPropertyMapper(AS_IS)
        .withPropertyToColumnMapper(AS_IS);

  static Connection connect() throws SQLException {
    HarvestConfig config = HarvestConfig.instance();
    String jdbcUrl = append(new StringBuilder(50),
          "jdbc:postgresql://",
          config.dbHost(),
          ":",
          config.dbPort(),
          "/",
          config.dbName()).toString();
    Properties props = new Properties();
    props.put("user", config.dbUser());
    props.put("password", config.dbPass());
    Connection con = DriverManager.getConnection(jdbcUrl, props);
    con.setAutoCommit(true);
    return con;
  }

  static void insertImages(Connection con, List<Image> images) {
    BatchInsert<Image> insert = SQL.insertBatch()
          .of(Image.class)
          .into("images")
          .withNameMapper(AS_IS)
          .prepare(con);
    insert.insertBatch(images);
  }

  static void insertImageLinks(Connection con, List<ImageLink> links) {
    BatchInsert<ImageLink> insert = SQL.insertBatch()
          .of(ImageLink.class)
          .into("imagelinks")
          .withNameMapper(AS_IS)
          .prepare(con);
    insert.insertBatch(links);
  }

  static Map<String, List<Specimen>> loadSpecimens(Connection con,
        List<Element> recordElems) {
    List<String> barcodes = recordElems.stream()
          .map(e -> XMLUtil.getDescendantValue(e, "dc:description"))
          .map(s -> StringMethods.substringBefore(s, "_", 1))
          .collect(toList());
    String sql = """
          SELECT specimenid,specimenbarcode
            FROM specimens
           WHERE specimenbarcode IN(~%barcodes%)
          """;
    SQL template = SQL.template(SESSION_CONFIG, sql);
    SQLSession session = template.session(con).setValue("barcodes", barcodes);
    int batchSize = HarvestConfig.instance().batchSize();
    Map<String, List<Specimen>> result = HashMap.newHashMap(batchSize + 25);
    try (SQLQuery query = session.prepareQuery()) {
      BeanExtractor<Specimen> extractor = query.getExtractor(Specimen.class);
      for (Specimen specimen : extractor) {
        String key = specimen.specimenbarcode();
        result.computeIfAbsent(key, k -> new ArrayList<>(4)).add(specimen);
      }
    }
    return result;
  }

  static UUID getDatabaseProjectId(Connection con) {
    String sql = "SELECT databaseprojectid FROM databaseprojects";
    SQL simple = SQL.simple(SESSION_CONFIG, sql);
    try (SQLQuery query = simple.session(con).prepareQuery()) {
      List<UUID> all = query.firstColumn(UUID.class);
      Check.that(all).has(size(), eq(), 1, illegalState("multiple database projects"));
      return all.get(0);
    }
  }

  static Set<String> findDuplicates(Connection con,
        List<Element> recordElems) {
    // If we are starting with a clean slate (an empty Brahms database,
    // or at least one where all medialib.naturalis.nl images have been
    // removed), we can't have duplicates.
    if (HarvestConfig.instance().repair()) {
      return Collections.emptySet();
    }
    List<String> imagefiles = recordElems.stream()
          .map(e -> XMLUtil.getDescendantValue(e, "dc:identifier"))
          .collect(toList());
    String sql = "SELECT imagefile FROM images WHERE imagefile IN (~%imagefiles%)";
    SQL template = SQL.template(SESSION_CONFIG, sql);
    SQLSession session = template.session(con).setValue("imagefiles", imagefiles);
    try (SQLQuery query = session.prepareQuery()) {
      return Set.copyOf(query.firstColumn());
    }
  }
}

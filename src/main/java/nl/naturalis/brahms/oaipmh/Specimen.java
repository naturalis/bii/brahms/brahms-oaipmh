package nl.naturalis.brahms.oaipmh;

import java.util.UUID;

public record Specimen(UUID specimenid, String specimenbarcode) { }


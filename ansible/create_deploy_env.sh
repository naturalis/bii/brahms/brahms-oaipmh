#!/bin/bash

# Creates the env file necessary for running the deployment
# locally rather than inside a Gitlab pipeline. This is not
# the env file for Docker Compose or the application! Also,
# it is about deploying _from_ your local machine; not about
# deploying _to_ your local machine.

f="${BASE_DIR}/ansible/deploy.env"

echo '# Check this file before every deployment!!! VAULT_TOKEN may need'                  >> "${f}"
echo '# to be refreshed as it expires. Go to HC Vault web UI to get a new'                >> "${f}"
echo '# token. Most other variables duplicate those found in .gitlab-ci.yml.'             >> "${f}"
echo '# CI_REGISTRY_PASSWORD is your Gitlab access token. Setting'                        >> "${f}"
echo '# IMAGE_VERSION is optional. By default, the "latest" image is'                     >> "${f}"
echo '# deployed (make sure that''s what you want!!!).'                                   >> "${f}"
echo ""                                                                                   >> "${f}"
echo "export VAULT_TOKEN="                                                                >> "${f}"
echo "export VAULT_SERVER_URL=https://vault.naturalis.io"                                 >> "${f}"
echo "export VAULT_PATH=bii/brahms/brahms-oaipmh"                                         >> "${f}"
echo "export ANSIBLE_GROUPS="                                                             >> "${f}"
echo "export ANSIBLE_HOSTS=dev"                                                           >> "${f}"
echo "export CI_REGISTRY=registry.gitlab.com"                                             >> "${f}"
echo "export CI_REGISTRY_USER="                                                           >> "${f}"
echo "export CI_REGISTRY_PASSWORD="                                                       >> "${f}"
echo "export IMAGE_VERSION="                                                              >> "${f}"

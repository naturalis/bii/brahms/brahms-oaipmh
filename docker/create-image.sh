#!/bin/bash

set -e

# Are we running inside a Gitlab pipeline?
GITLAB_PIPELINE=true
[[ -z ${CI_JOB_ID+x} ]] && GITLAB_PIPELINE=false

if [[ ${GITLAB_PIPELINE} == true ]]; then
  BASE_DIR="${CI_PROJECT_DIR}"
else
  BASE_DIR=$(dirname "${0}")/../
  BASE_DIR=$(realpath "${BASE_DIR}")
fi

echo "Creating Docker image for Git commit \"${CI_COMMIT_SHORT_SHA}\""

image_url="${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}"
echo "===> Image URL: ${image_url}"

echo '===> Deleting older version of image (if present)'
docker image rm --force ${image_url} &> /dev/null

echo '===> Creating image'
docker build \
      --no-cache \
      --pull \
      --tag "${image_url}" \
      -f "${BASE_DIR}/docker/Dockerfile" \
      "${BASE_DIR}/docker"

echo '===> Authenticating GitLab container registry'
echo -n ${CI_REGISTRY_PASSWORD} | docker login -u ${CI_REGISTRY_USER} --password-stdin ${CI_REGISTRY} &> /dev/null

echo '===> Pushing image to container registry'
docker push "${image_url}" &> /dev/null

echo '===> Image created and pushed'
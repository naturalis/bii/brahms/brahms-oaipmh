<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <modelVersion>4.0.0</modelVersion>

    <groupId>org.klojang</groupId>
    <artifactId>brahms-oaipmh</artifactId>
    <version>1.0.0</version>
    <packaging>jar</packaging>

    <name>Brahms OAIPMH client</name>
    <description>
        Harvests the OAIPMH server in order to enrich
        Brahms with image metadata from the MediaLib
    </description>
    <url>https://gitlab.com/naturalis/bii/brahms/brahms-oaipmh</url>

    <licenses>
        <license>
            <name>GPL-3.0 license</name>
            <url>https://www.gnu.org/licenses/gpl-3.0.en.html</url>
        </license>
    </licenses>

    <developers>
        <developer>
            <name>Ayco Holleman</name>
            <email>ayco.holleman@naturalis.nl</email>
            <organization>Naturalis Biodiversity Centre</organization>
            <organizationUrl>https://www.naturalis.nl/</organizationUrl>
        </developer>
    </developers>

    <organization>
        <name>Naturalis Biodiversity Center</name>
        <url>http://www.naturalis.nl</url>
    </organization>

    <scm>
        <connection>scm:git:https://gitlab.com/naturalis/bii/brahms/brahms-oaipmh.git</connection>
        <developerConnection>scm:git:https://gitlab.com/naturalis/bii/brahms/brahms-oaipmh.git</developerConnection>
        <url>https://gitlab.com/naturalis/bii/brahms/brahms-oaipmh</url>
    </scm>

    <properties>
        <!-- ==================================================================== -->
        <!-- *** Build settings                                               *** -->
        <!-- ==================================================================== -->
        <maven.compiler.source>21</maven.compiler.source>
        <maven.compiler.target>21</maven.compiler.target>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

        <!-- ==================================================================== -->
        <!-- *** Maven plugin versions                                        *** -->
        <!-- *** Check for updates: mvn versions:display-plugin-updates       *** -->
        <!-- ==================================================================== -->
        <maven-clean-plugin.version>3.3.2</maven-clean-plugin.version>
        <maven-enforcer-plugin.version>3.4.1</maven-enforcer-plugin.version>
        <maven-checkstyle-plugin.version>3.3.1</maven-checkstyle-plugin.version>
        <maven-compiler-plugin.version>3.12.1</maven-compiler-plugin.version>
        <maven-dependency-plugin.version>3.6.1</maven-dependency-plugin.version>
        <maven-javadoc-plugin.version>3.6.3</maven-javadoc-plugin.version>
        <maven-source-plugin.version>3.3.0</maven-source-plugin.version>
        <maven-resources-plugin.version>3.3.1</maven-resources-plugin.version>
        <maven-surefire-plugin.version>3.2.5</maven-surefire-plugin.version>
        <maven-deploy-plugin.version>3.1.1</maven-deploy-plugin.version>
        <maven-gpg-plugin.version>3.1.0</maven-gpg-plugin.version>
        <git-commit-id-plugin.version>4.9.10</git-commit-id-plugin.version>
        <jacoco-plugin.version>0.8.11</jacoco-plugin.version>
        <owasp-plugin.version>9.0.3</owasp-plugin.version>


        <!-- ==================================================================== -->
        <!-- *** Library dependencies                                         *** -->
        <!-- *** Check for updates: mvn versions:display-dependency-updates   *** -->
        <!-- *** Check dependencies: mvn dependency:analyze                   *** -->
        <!-- ==================================================================== -->
        <klojang-check.version>3.0.3-jdk21</klojang-check.version>
        <klojang-util.version>1.1.8</klojang-util.version>
        <klojang-jdbc.version>1.1.5</klojang-jdbc.version>
        <postgres.version>42.7.2</postgres.version>
        <quartz.version>2.5.0-rc1</quartz.version>
        <slf4j.version>2.0.9</slf4j.version>
        <logback.version>1.5.0</logback.version>
        <junit.version>5.10.2</junit.version>

    </properties>


    <dependencies>
        <dependency>
            <groupId>org.klojang</groupId>
            <artifactId>klojang-check</artifactId>
            <version>${klojang-check.version}</version>
        </dependency>
        <dependency>
            <groupId>org.klojang</groupId>
            <artifactId>klojang-util</artifactId>
            <version>${klojang-util.version}</version>
        </dependency>
        <dependency>
            <groupId>org.klojang</groupId>
            <artifactId>klojang-jdbc</artifactId>
            <version>${klojang-jdbc.version}</version>
        </dependency>
        <dependency>
            <groupId>org.postgresql</groupId>
            <artifactId>postgresql</artifactId>
            <version>${postgres.version}</version>
        </dependency>
        <dependency>
            <groupId>org.quartz-scheduler</groupId>
            <artifactId>quartz</artifactId>
            <version>${quartz.version}</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>${slf4j.version}</version>
        </dependency>
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
            <version>${logback.version}</version>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter</artifactId>
            <version>${junit.version}</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <filtering>true</filtering>
            </resource>
        </resources>
        <testResources>
            <testResource>
                <directory>src/test/resources</directory>
                <filtering>false</filtering>
            </testResource>
        </testResources>

        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-clean-plugin</artifactId>
                    <version>${maven-clean-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-enforcer-plugin</artifactId>
                    <version>${maven-enforcer-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-checkstyle-plugin</artifactId>
                    <version>${maven-checkstyle-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-source-plugin</artifactId>
                    <version>${maven-source-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-resources-plugin</artifactId>
                    <version>${maven-resources-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>${maven-compiler-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-dependency-plugin</artifactId>
                    <version>${maven-dependency-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>pl.project13.maven</groupId>
                    <artifactId>git-commit-id-plugin</artifactId>
                    <version>${git-commit-id-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <version>${maven-surefire-plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-javadoc-plugin</artifactId>
                    <version>${maven-javadoc-plugin.version}</version>
                </plugin>
            </plugins>
        </pluginManagement>

        <plugins>
            <!--plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-enforcer-plugin</artifactId>
                <executions>
                    <execution>
                        <id>enforce-versions</id>
                        <goals>
                            <goal>enforce</goal>
                        </goals>
                        <configuration>
                            <rules>
                                <requireMavenVersion>
                                    <version>[3.6.3,)</version>
                                </requireMavenVersion>
                                <requireJavaVersion>
                                    <version>[17,)</version>
                                </requireJavaVersion>
                            </rules>
                        </configuration>
                    </execution>
                </executions>
            </plugin-->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-clean-plugin</artifactId>
                <configuration>
                    <filesets>
                        <fileset>
                            <directory>docker/dependencies</directory>
                            <includes>
                                <include>**/*.jar</include>
                            </includes>
                        </fileset>
                    </filesets>
                </configuration>
            </plugin>
            <!--plugin>
                <groupId>pl.project13.maven</groupId>
                <artifactId>git-commit-id-plugin</artifactId>
                <configuration>
                    <dotGitDirectory>${project.basedir}/.git</dotGitDirectory>
                    <generateGitPropertiesFile>true</generateGitPropertiesFile>
                    <generateGitPropertiesFilename>
                        ${project.build.outputDirectory}/${project.artifactId}.git.properties
                    </generateGitPropertiesFilename>
                    <format>properties</format>
                    <dateFormat>yyyy-MM-dd HH:mm:ss</dateFormat>
                </configuration>
                <executions>
                    <execution>
                        <id>generate-git-properties</id>
                        <phase>initialize</phase>
                        <goals>
                            <goal>revision</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin-->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-checkstyle-plugin</artifactId>
                <executions>
                    <execution>
                        <id>check-style</id>
                        <configuration>
                            <configLocation>google_checks.xml</configLocation>
                            <failsOnError>true</failsOnError>
                            <consoleOutput>true</consoleOutput>
                            <includeTestSourceDirectory>true
                            </includeTestSourceDirectory>
                        </configuration>
                        <goals>
                            <goal>check</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <source>${maven.compiler.source}</source>
                    <target>${maven.compiler.target}</target>
                    <encoding>${project.build.sourceEncoding}</encoding>
                </configuration>
            </plugin>
            <!--plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>${jacoco-plugin.version}</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>report</id>
                        <phase>prepare-package</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin-->
            <!--plugin>
                <groupId>org.owasp</groupId>
                <artifactId>dependency-check-maven</artifactId>
                <version>${owasp-plugin.version}</version>
                <configuration>
                    <failBuildOnCVSS>7</failBuildOnCVSS>
                    <nvdApiKey>29bb9b20-8dba-4ae7-94b4-6656e154e6e1</nvdApiKey>
                </configuration>
                <executions>
                    <execution>
                        <phase>verify</phase>
                        <goals>
                            <goal>check</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin-->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <version>${maven-source-plugin.version}</version>
                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <goals>
                            <goal>jar-no-fork</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-dependency-plugin</artifactId>
                <configuration>
                    <outputDirectory>
                        docker/dependencies
                    </outputDirectory>
                    <includeScope>compile</includeScope>
                </configuration>
                <executions>
                    <execution>
                        <id>copy-dependencies</id>
                        <phase>install</phase>
                        <goals>
                            <goal>copy-dependencies</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>copy-artifact</id>
                        <phase>install</phase>
                        <goals>
                            <goal>copy</goal>
                        </goals>
                        <configuration>
                            <artifactItems>
                                <artifactItem>
                                    <groupId>${project.groupId}</groupId>
                                    <artifactId>${project.artifactId}</artifactId>
                                    <version>${project.version}</version>
                                    <type>jar</type>
                                    <overWrite>true</overWrite>
                                </artifactItem>
                            </artifactItems>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

</project>
